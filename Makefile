NAME	= woody

CC	= gcc
DEBUG	= -g
CFLAGS	= -Wall -Wextra $(DEBUG)#-Werror

AS	= nasm
ASFLAGS = -f elf64 -F dwarf

OBJ_FILES =	woody.o		\
		file.o		\
		check.o		\
		corrupt.o	\
		foreach.o	\
		entry.o		\
		header.o	\
		utils.o		\
		write.o		\
		encrypt.o	\
		encryption.o	\
		payload.o


INC_FILES	= bash_colors.h error.h utils.h woody.h

SRC_FOLDER	= src/
OBJ_FOLDER	= build/
INC_FOLDER	= include/

INC	= $(addprefix $(INC_FOLDER), $(INC_FILES))
OBJ	= $(addprefix $(OBJ_FOLDER), $(OBJ_FILES))
SRC	= $(addprefix $(SRC_FOLDER), $(OBJ_FILES:.o=.c))

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(INCS)

$(OBJ_FOLDER)%.o: $(SRC_FOLDER)%.c $(INC)
	@mkdir -p $(OBJ_FOLDER)
	$(CC) $(CFLAGS) -c $< -o $@ -I./$(INC_FOLDER)

$(OBJ_FOLDER)%.o: $(SRC_FOLDER)%.s
	@mkdir -p $(OBJ_FOLDER)
	$(AS) $(ASFLAGS) $< -o $@

clean:
	@rm -rf $(OBJ_FOLDER)

fclean: clean
	@rm -f $(NAME)

re:
	make fclean
	make all

prout:
	gcc -c toto.c
	nasm -f elf64 -F dwarf aess.s
	nasm -f elf64 -F dwarf encrypt.s
	gcc -o toto toto.o aess.o encrypt.o
