#ifndef ERROR_H
#define ERROR_H

#include <bash_colors.h>
#include <utils.h>
#include <errno.h>

#define ERR		RED "** FATAL ERROR **\n" DEF
#define WARN		MAG "** WARNING **\n" DEF
#define CORRUPT		YEL "[** CORRUPTED **]\n" DEF
#define INJECT		YEL "[** INJECTION **]\n" DEF

#define NORET		0

#define ERR_FILE	PROG_NAME ": "

#define EB_ISDIR	"Is a directory"
#define EB_ISNREG	"Is not a regular file"
#define EB_FILETOOBIG	"fstat gives negative st_size, probably too big"
#define EB_FILEEMPTY	"File is empty"

#define ECOR_HDRS	"Header size is too small"
#define ECOR_MAGIC	"Elf file has wrong magic number"
#define ECOR_TYPE	"Elf file has no file type"
#define ECOR_VERSION	"Invalid Elf version"
#define ECOR_MACHINE	"Only AMD x86-64 architecture is supported"

#define ECOR_SPHENTSIZE	"The e_phentsize entry is smaller than the size of an Elf program header"
#define ECOR_LPHENTSIZE	"The e_phentsize entry is larger than the size of an Elf program header"

#define ECOR_SSHENTSIZE	"The e_shentsize entry is smaller than the size of an Elf section header"
#define ECOR_LSHENTSIZE	"The e_shentsize entry is larger than the size of an Elf section header"

#define ECOR_PHNUM	"Too many program headers, the file is too small"
#define ECOR_SHNUM	"Too many section headers, the file is too small"

#define ECOR_PHENTRY	"Entry point is not in a valid segment"
#define ECOR_SHENTRY	"Entry point is not in a valid section"

#define ECOR_ALIGN	"Text section offset needs to be aligned on 16 bytes"

#define EINJ_SPACE	"The space between the two segments is insufficient to contain the payload"

enum e_errors
{
	ERR_ERRNO,
	ERR_USAGE,
	ERR_WARN,
	ERR_BASIC,
	ERR_CORRUPT,
	ERR_INJECT,
	/* Always keep this one last */
	ERR_MAX
};

#define error_basic(ret, file, err) ({					\
	error(ret, ERR_BASIC, "%s: %s", file, err);			\
})

#define error_errno(ret, file) ({					\
	error(ret, ERR_ERRNO, "%s: ", file);				\
})

#define error_warn(ret, file, fmt, ...) ({				\
	error(ret, ERR_WARN, "%s: " fmt, file, ##__VA_ARGS__);		\
})

#define error_usage(ret) ({						\
	error(ret, ERR_USAGE, "");					\
})

#define error_corrupt(ret, fmt, ...) ({					\
	error(ret, ERR_CORRUPT, "%s: " fmt, origin.name, ##__VA_ARGS__);\
})

#define error_injection(ret, fmt, ...) ({				\
	error(ret, ERR_INJECT, "%s: " fmt, origin.name, ##__VA_ARGS__);\
})

#define error(ret, type, fmt, ...) ({						\
	static const char *err[] = {						\
		[ERR_ERRNO] = ERR,						\
		[ERR_USAGE] = ERR "Usage: ./woody [FILE]",			\
		[ERR_WARN] = WARN,						\
		[ERR_BASIC] = ERR,						\
		[ERR_CORRUPT] = ERR CORRUPT,					\
		[ERR_INJECT] = ERR INJECT					\
	};									\
	static_assert(COUNT_OF(err) == ERR_MAX, "error.h, wrong size!");	\
	const char *err_errno = (type == ERR_ERRNO ? strerror(errno) : "");	\
	const char *prog_name = (type != ERR_USAGE ? ERR_FILE : "");		\
	fflush(stdout);								\
	fprintf(stderr, "%s%s" fmt "%s\n", err[type],				\
			prog_name, ##__VA_ARGS__, err_errno);			\
	(ret);									\
})

#endif
