#ifndef WOODY_H
#define WOODY_H

#include <elf.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <error.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <stddef.h>

#define PROG_NAME "woody"

typedef struct
{
	const char	*name;
	void		*memptr;
	ssize_t		size;

	Elf64_Addr entry;

	Elf64_Ehdr *hdr;
	Elf64_Shdr *sh;
	Elf64_Phdr *ph;
	
	Elf64_Phdr *phtxt;
	Elf64_Shdr *shtxt;
	
	Elf64_Half phtext_id;
	Elf64_Half shtext_id;
} t_file;

typedef struct
{
	Elf64_Ehdr *hdr;
	Elf64_Shdr *sh;
	Elf64_Phdr *ph;
} t_output;

typedef struct
{
	unsigned long decrypt_addr;
	unsigned long size;
	unsigned char key[16];
	unsigned int jmp_addr;
} t_payload;

typedef int (*f_foreach_ph_call)(Elf64_Phdr *ph, Elf64_Half i);
typedef int (*f_foreach_sh_call)(Elf64_Shdr *sh, Elf64_Half i);

extern t_file	origin;
extern t_output	out;

extern unsigned long	_payload_size;
extern unsigned long	_settings_size;
extern void		_entry(void);
extern void		_jump(void);
extern unsigned long	_jmp_size;

int	map_file(t_file *file, const char *name);

int	check_file_header(const t_file *file);

int	check_corrupt(ssize_t offset, ssize_t size);

int	foreach_segment(Elf64_Phdr *ph, Elf64_Half n, f_foreach_ph_call);
int	foreach_section(Elf64_Shdr *sh, Elf64_Half n, f_foreach_sh_call);

int	get_entry(t_file *file);

void	*memdup(void *src, size_t n);

int	modify_headers(void);
int	generate_key(unsigned char *key);
void	encrypt(unsigned char *key);
int	write_outfile(unsigned char *key);

void	_expand_key(unsigned char *expkey, const unsigned char *key);
void	_aes_encrypt(unsigned char *msg, unsigned char *key, size_t size);

#endif
