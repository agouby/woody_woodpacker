#ifndef UTILS_H
#define UTILS_H

#define ALIGN(n, fac) ((n + fac) & (~(fac - 1)))

#define COUNT_OF(x) (sizeof(x) / sizeof(*x))

#define PAGE_SIZE	0x1000

#define KEY_SIZE	0x10
#define EXPKEY_SIZE	0xb0

#define __unused__ __attribute__((unused))

#endif
