#include <woody.h>

static void print_key(unsigned char *key)
{
	printf("[KEY]: ");
	for (size_t i = 0; i < KEY_SIZE; i++)
	{
		printf("%.2X", key[i]);
		(i != KEY_SIZE - 1) ? printf(" "): printf("\n");
	}
}

int	generate_key(unsigned char *key)
{
	int fd;

	fd = open("/dev/urandom", O_RDONLY);
	if (fd == -1)
		return error_errno(-1, origin.name);
	if (read(fd, key, KEY_SIZE) == -1)
		return error_errno(-1, origin.name);
	print_key(key);
	return 0;
}

void	encrypt(unsigned char *key)
{
	Elf64_Shdr *sh;
	unsigned char expkey[EXPKEY_SIZE];

	_expand_key(expkey, key);
	sh = &origin.sh[origin.shtext_id];
	_aes_encrypt(origin.memptr + sh->sh_offset, expkey, ALIGN(sh->sh_size, 0x10));
}
