#include <woody.h>

static void	set_payload_infos(t_payload *settings, unsigned char *key)
{
	Elf64_Phdr *phtext;
	Elf64_Shdr *shtext;
	Elf64_Phdr *ori_phtext;

	phtext = &out.ph[origin.phtext_id];
	shtext = &origin.sh[origin.shtext_id];
	ori_phtext = &origin.ph[origin.phtext_id];

	settings->jmp_addr = phtext->p_vaddr + phtext->p_memsz;
	settings->jmp_addr = origin.hdr->e_entry - settings->jmp_addr;

	settings->decrypt_addr = ALIGN(ori_phtext->p_offset + ori_phtext->p_memsz, 0x10) - shtext->sh_offset;
	settings->size = ALIGN(shtext->sh_size, 0x10);

	memcpy(settings->key, key, KEY_SIZE);

}

static void	write_payload(int fd, unsigned char *key)
{
	off_t payload_addr;
	t_payload settings;

	set_payload_infos(&settings, key);
	
	payload_addr = origin.phtxt->p_offset + ALIGN(origin.phtxt->p_filesz, 0x10);
	
	lseek(fd, payload_addr, SEEK_SET);
	write(fd, _entry, _payload_size - _settings_size);

	write(fd, &settings, sizeof(t_payload) - 0x8);
	write(fd, _jump, 1);
	write(fd, &settings.jmp_addr, sizeof(settings.jmp_addr));
}

int	write_outfile(unsigned char *key)
{
	int fd;

	fd = open("new", O_RDWR | O_TRUNC | O_CREAT,0755);
	if (fd == -1)
		return error_errno(-1, origin.name);

	write(fd, out.hdr, sizeof(Elf64_Ehdr));
	write(fd, out.ph, sizeof(Elf64_Phdr) * out.hdr->e_phnum);

	for (int i = 0; i < out.hdr->e_shnum; i++) {
		lseek(fd, out.sh[i].sh_offset, SEEK_SET);
		write(fd, origin.memptr + origin.sh[i].sh_offset, out.sh[i].sh_size);
	}
	lseek(fd, out.hdr->e_shoff, SEEK_SET);
	write(fd, out.sh, sizeof(Elf64_Shdr) * out.hdr->e_shnum);

	write_payload(fd, key);
	return 0;
}
