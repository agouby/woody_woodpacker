%define EXPKEY	rdi
%define TMPKEY	xmm0
%define STATE	xmm1
%define KEY	rsi
%define MSG	rdi
%define SIZE	rdx

section .text

extern _aes_encrypt
extern _expand_key

;				~~[[ _expand_key ]]~~
;	
;	void	_expand_key(unsigned char *expkey, const unsigned char *key);
;			
;	expkey: 176 bytes uninitialized array	(rdi)	(RW)
;	key:	16 bytes original key		(rsi)	(RO)


;				~~[[ _aes_encrypt ]]~~
;	
;	void	_aes_encrypt(unsigned char *msg, unsigned char *key, size_t size);
;			
;	msg:	size bytes buffer		(rdi)	(RW)	(ALIGNED 16)
;	key:	176 bytes expanded key		(rsi)	(RO)
;	size:	size of the message		(rdx)	(RO)	(ALIGNED 16)


_aes_encrypt:
	
	movaps STATE, [MSG]
	call _encrypt
	movaps [MSG], STATE
	add MSG, 0x10
	sub SIZE, 0x10
	test SIZE, SIZE
	jnz _aes_encrypt
	ret

_encrypt:
	movaps TMPKEY, [KEY]

	pxor STATE, TMPKEY

	movaps TMPKEY, [KEY + 0x10]
	aesenc STATE, TMPKEY
	movaps TMPKEY, [KEY + 0x20]
	aesenc STATE, TMPKEY
	movaps TMPKEY, [KEY + 0x30]
	aesenc STATE, TMPKEY
	movaps TMPKEY, [KEY + 0x40]
	aesenc STATE, TMPKEY
	movaps TMPKEY, [KEY + 0x50]
	aesenc STATE, TMPKEY
	movaps TMPKEY, [KEY + 0x60]
	aesenc STATE, TMPKEY
	movaps TMPKEY, [KEY + 0x70]
	aesenc STATE, TMPKEY
	movaps TMPKEY, [KEY + 0x80]
	aesenc STATE, TMPKEY
	movaps TMPKEY, [KEY + 0x90]
	aesenc STATE, TMPKEY
	
	movaps TMPKEY, [KEY + 0xa0]
	aesenclast STATE, TMPKEY
	
	ret	

_expand_key:
	; Copy the first 16 bytes key
	movaps xmm0, [KEY]
	movaps [EXPKEY], xmm0
	add EXPKEY, 0x10

	pxor xmm4, xmm4

	aeskeygenassist xmm1, xmm0, 0x01
	call _keygen_step
	aeskeygenassist xmm1, xmm0, 0x02
	call _keygen_step
	aeskeygenassist xmm1, xmm0, 0x04
	call _keygen_step
	aeskeygenassist xmm1, xmm0, 0x08
	call _keygen_step
	aeskeygenassist xmm1, xmm0, 0x10
	call _keygen_step
	aeskeygenassist xmm1, xmm0, 0x20
	call _keygen_step
	aeskeygenassist xmm1, xmm0, 0x40
	call _keygen_step
	aeskeygenassist xmm1, xmm0, 0x80
	call _keygen_step
	aeskeygenassist xmm1, xmm0, 0x1b
	call _keygen_step
	aeskeygenassist xmm1, xmm0, 0x36
	call _keygen_step

	ret

_keygen_step:
	pshufd xmm1, xmm1, 0b11111111
	shufps xmm4, xmm0, 0b00010000
	pxor xmm0, xmm4
	shufps xmm4, xmm0, 0b10001100
	pxor xmm0, xmm4
	pxor xmm0, xmm1
	movaps [EXPKEY], xmm0
	add EXPKEY, 0x10
	ret
