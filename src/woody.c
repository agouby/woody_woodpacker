#include <woody.h>

t_file origin = {0};
t_output out = {0};

int	main(int ac, char **av)
{
	unsigned char key[KEY_SIZE];
	int ret = EXIT_SUCCESS;

	if (ac != 2)
		return error_usage(EXIT_FAILURE);
	
	if (map_file(&origin, av[1]) == -1)
		return EXIT_FAILURE;

	if (check_file_header(&origin) == -1) {
		ret = EXIT_FAILURE;
		goto _exit;
	}

	if (get_entry(&origin) == -1) {
		ret = EXIT_FAILURE;
		goto _exit;
	}

	if (modify_headers() == -1) {
		ret = EXIT_FAILURE;
		goto _exit;
	}

	if (generate_key(key) == -1) {
		ret = EXIT_FAILURE;
		goto _exit;
	}

	encrypt(key);

	if (write_outfile(key) == -1) {
		ret = EXIT_FAILURE;
		goto _exit;
	}

_exit:
	munmap(origin.memptr, origin.size);
	free(out.hdr);
	free(out.sh);
	free(out.ph);
	return ret;
}
