#include <woody.h>

static ssize_t get_file_size(const int fd, const char *name)
{
	struct stat st;
	
	if (fstat(fd, &st) == -1)
		return error_errno(-1, name);
	if (S_ISDIR(st.st_mode))
		return error_basic(-1, name, EB_ISDIR);
	if (!S_ISREG(st.st_mode))
		return error_basic(-1, name, EB_ISNREG);
	if (st.st_size < 0)
		return error_basic(-1, name, EB_FILETOOBIG);
	if (st.st_size == 0)
		return error_basic(-1, name, EB_FILEEMPTY);
	
	return (ssize_t)st.st_size;
}

int	map_file(t_file *file, const char *name)
{
	int fd;
	void *ret;

	fd = open(name, O_RDONLY);
	if (fd == -1)
		return error_errno(-1, name);
	
	file->size = get_file_size(fd, name);
	if (file->size == -1) {
		close(fd);
		return -1;
	}

	ret = mmap(0, file->size, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
	if (ret == MAP_FAILED) {
		close(fd);
		return error_errno(-1, name);
	}
	close(fd);

	file->name = name;
	file->memptr = ret;
	return 0;
}
