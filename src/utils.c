#include <woody.h>

void	*memdup(void *src, size_t n)
{
	void *ret;

	ret = malloc(n);
	if (ret == NULL)
		return NULL;
	memcpy(ret, src, n);
	return ret;
}
