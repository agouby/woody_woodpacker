#include <woody.h>

int	foreach_segment(Elf64_Phdr *ph, Elf64_Half n, f_foreach_ph_call func)
{
	for (Elf64_Half i = 0; i < n; i++) {
		if (func(&ph[i], i) == -1)
			return -1;
	}
	return 0;
}

int	foreach_section(Elf64_Shdr *sh, Elf64_Half n, f_foreach_sh_call func)
{
	for (Elf64_Half i = 0; i < n; i++) {
		if (func(&sh[i], i) == -1)
			return -1;
	}
	return 0;
}
