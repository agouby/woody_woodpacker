#include <woody.h>

static int	modify_segment(Elf64_Phdr *ph, __unused__ Elf64_Half i)
{
	if (ph->p_offset > origin.ph[origin.phtext_id].p_offset)
		ph->p_offset += PAGE_SIZE;
	return 0;
}

static int	modify_section(Elf64_Shdr *sh, __unused__ Elf64_Half i)
{
	if (sh->sh_offset > origin.sh[origin.shtext_id].sh_offset)
		sh->sh_offset += PAGE_SIZE;
	return 0;
}

static int	check_available_space(Elf64_Phdr *phtext)
{
	Elf64_Phdr *next_ph;

	next_ph = &origin.ph[origin.phtext_id + 1];

	if (origin.hdr->e_phnum == origin.phtext_id + 1)
		return 0;
	if (phtext->p_vaddr + phtext->p_memsz > next_ph->p_vaddr)
		return error_injection(-1, EINJ_SPACE);
	return 0;
}



static int	copy_headers(void)
{
	out.hdr = memdup(origin.hdr, sizeof(Elf64_Ehdr));
	if (!out.hdr)
		return error_errno(-1, origin.name);
	out.sh = memdup(origin.sh, sizeof(Elf64_Shdr) * origin.hdr->e_shnum);
	if (!out.sh)
		return error_errno(-1, origin.name);
	out.ph = memdup(origin.ph, sizeof(Elf64_Phdr) * origin.hdr->e_phnum);
	if (!out.ph)
		return error_errno(-1, origin.name);
	return 0;
}

int		modify_headers(void)
{
	Elf64_Shdr *shtext;
	Elf64_Phdr *phtext;

	if (copy_headers() == -1)
		return -1;

	shtext = &out.sh[origin.shtext_id];
	phtext = &out.ph[origin.phtext_id];
	origin.phtxt = &origin.ph[origin.phtext_id];
	origin.shtxt = &origin.sh[origin.shtext_id];

	out.hdr->e_shoff += PAGE_SIZE;
	out.hdr->e_entry = origin.phtxt->p_vaddr + ALIGN(origin.phtxt->p_memsz, 0x10);

	shtext->sh_size = ALIGN(shtext->sh_size, 0x10) + _payload_size;
	phtext->p_memsz = ALIGN(phtext->p_memsz, 0x10) + _payload_size;
	phtext->p_filesz = ALIGN(phtext->p_filesz, 0x10) + _payload_size;
	phtext->p_flags |= PF_W;

	if (check_available_space(phtext) == -1)
		return -1;

	foreach_section(out.sh, out.hdr->e_shnum, modify_section);
	foreach_segment(out.ph, out.hdr->e_phnum, modify_segment);

	return 0;
}
