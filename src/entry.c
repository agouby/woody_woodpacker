#include <woody.h>

static int	get_entry_segment(Elf64_Phdr *ph, Elf64_Half i)
{
	if (ph->p_vaddr <= origin.entry)
		if (origin.entry < ph->p_vaddr + ph->p_filesz)
			origin.phtext_id = i;
	return 0;
}

static int	get_entry_section(Elf64_Shdr *sh, Elf64_Half i)
{
	if (sh->sh_addr && sh->sh_addr <= origin.entry)
		if (origin.entry < sh->sh_addr + sh->sh_size)
			origin.shtext_id = i;
	return 0;
}

static int check_offset_aligned(void)
{
	Elf64_Shdr *sh;

	sh = &origin.sh[origin.shtext_id];
	if (sh->sh_offset & 0xF)
		return error_corrupt(-1, ECOR_ALIGN);
	return 0;
}

int	get_entry(t_file *file)
{
	Elf64_Ehdr *hdr = (Elf64_Ehdr *)file->memptr;
	Elf64_Shdr *sh = (void *)hdr + hdr->e_shoff;
	Elf64_Phdr *ph = (void *)hdr + hdr->e_phoff;
	
	file->entry = hdr->e_entry;
	file->hdr = hdr;
	file->sh = sh;
	file->ph = ph;
	
	foreach_segment(ph, hdr->e_phnum, get_entry_segment);
	if (file->phtext_id == 0)
		return error_corrupt(-1, ECOR_PHENTRY);
	
	foreach_section(sh, hdr->e_shnum, get_entry_section);
	if (file->shtext_id == 0)
		return error_corrupt(-1, ECOR_SHENTRY);
	
	if (check_offset_aligned() == -1)
		return -1;
	return 0;
}
