#include <woody.h>

int	check_corrupt(ssize_t offset, ssize_t size)
{
	if (offset + size <= origin.size && offset + size >= offset)
		return 0;
	return -1;
}
