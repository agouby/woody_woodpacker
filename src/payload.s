%define KEYPTR	r8
%define KEYSTOP	r9
%define KEY	xmm0
%define EXPKEY	r11
%define STATE	xmm1
%define MSG	rdx
%define MSGOUT	r10
%define SIZE	rcx

DEFAULT rel

extern _payload_size
extern _settings_size
extern _jump
extern _entry

_payload_size dq _end - _entry
_settings_size dq _end - _settings

_entry:
	call _get_rip
	sub r13, 5
	push rbp
	mov rbp, rsp
;	sub rsp, 0x8 ; REMOVE IF ALIGNMENT IS NOT NEEDED ANYMORE
	
	pushf
        push rax
        push rbx
        push rcx
        push rdx
        push rsi
        push rdi
        push r8
        push r9
        push r10
        push r11
        push r12
        push r13
        push r14
        push r15

	mov rax, 0x1
	mov rdi, 0x1
	lea rsi, [rel woody]
	mov rdx, [woody_len]
	syscall

_expand_key:

	sub rsp, 0xb0

	mov EXPKEY, rsp
	; Copy the first 16 bytes key
	movaps xmm0, [key]
	movaps [EXPKEY], xmm0
        add EXPKEY, 0x10

        pxor xmm4, xmm4

        aeskeygenassist xmm1, xmm0, 0x01
        call _keygen_step
        aeskeygenassist xmm1, xmm0, 0x02
        call _keygen_step
        aeskeygenassist xmm1, xmm0, 0x04
        call _keygen_step
        aeskeygenassist xmm1, xmm0, 0x08
        call _keygen_step
        aeskeygenassist xmm1, xmm0, 0x10
        call _keygen_step
        aeskeygenassist xmm1, xmm0, 0x20
        call _keygen_step
        aeskeygenassist xmm1, xmm0, 0x40
        call _keygen_step
        aeskeygenassist xmm1, xmm0, 0x80
        call _keygen_step
        aeskeygenassist xmm1, xmm0, 0x1b
        call _keygen_step
        aeskeygenassist xmm1, xmm0, 0x36
        call _keygen_step
	jmp _aes_decrypt

_keygen_step:
        pshufd xmm1, xmm1, 0b11111111
        shufps xmm4, xmm0, 0b00010000
        pxor xmm0, xmm4
        shufps xmm4, xmm0, 0b10001100
        pxor xmm0, xmm4
        pxor xmm0, xmm1
        movaps [EXPKEY], xmm0
        add EXPKEY, 0x10
        ret

_aes_decrypt:
	lea KEYPTR, [rsp + 0x10]
	lea KEYSTOP, [rsp + 0xa0]
	call _key_invloop

	mov SIZE, [size]
	mov MSG, r13
	sub MSG, [decrypt_addr]

	; Start of loop (Goes through the message 16 bytes at a time)

_aes_decrypt_loop:
	movaps STATE, [MSG]		; USED AS STATE
	call _decrypt
	movaps [MSG], STATE
	add MSG, 0x10
	add MSGOUT, 0x10
	sub SIZE, 0x10
	test SIZE, SIZE
	jnz _aes_decrypt_loop

	; End of loop

_exit:
	add rsp, 0xb0
	pop r15
        pop r14
        pop r13
        pop r12
        pop r11
        pop r10
        pop r9
        pop r8
        pop rdi
        pop rsi
        pop rdx
        pop rcx
        pop rbx
        pop rax
        popf

	mov rsp, rbp
	pop rbp
	jmp _jump
	
_decrypt:
	movaps KEY, [KEYPTR]		; USED AS KEY
	pxor STATE, KEY			; ADD ROUND KEY ($RBP - 0xa0)

	; Do 9 rounds
	movaps KEY, [KEYPTR - 0x10]
	aesdec STATE, KEY
	movaps KEY, [KEYPTR - 0x20]
	aesdec STATE, KEY
	movaps KEY, [KEYPTR - 0x30]
	aesdec STATE, KEY
	movaps KEY, [KEYPTR - 0x40]
	aesdec STATE, KEY
	movaps KEY, [KEYPTR - 0x50]
	aesdec STATE, KEY
	movaps KEY, [KEYPTR - 0x60]
	aesdec STATE, KEY
	movaps KEY, [KEYPTR - 0x70]
	aesdec STATE, KEY
	movaps KEY, [KEYPTR - 0x80]
	aesdec STATE, KEY
	movaps KEY, [KEYPTR - 0x90]
	aesdec STATE, KEY

	; Do last round
	movaps KEY, [KEYPTR - 0xA0]
	aesdeclast STATE, KEY
	ret

_key_invloop:
	aesimc KEY, [KEYPTR]
	movaps [KEYPTR], KEY
	add KEYPTR, 0x10
	cmp KEYPTR, KEYSTOP
	jne _key_invloop
	ret

	woody db "...WOODY...",0xa
	woody_len dq $ - woody

_get_rip:
	mov r13, [rsp]
	ret
	
align 16
_settings:
	decrypt_addr dq 0x0
	size dq 0x0
	key times 2 dq 0x0
_jump:
	jmp 0xFFFFFFFF
_end:
