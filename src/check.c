#include <woody.h>

int	check_file_header(const t_file *file)
{
	Elf64_Ehdr	*hdr;

	if (check_corrupt(0, sizeof(Elf64_Ehdr)))
		return error_corrupt(-1, ECOR_HDRS);

	hdr = (Elf64_Ehdr *)file->memptr;

	if (memcmp(hdr->e_ident, ELFMAG, SELFMAG))
		return error_basic(-1, file->name, ECOR_MAGIC);

	if (hdr->e_type == ET_NONE)
		return error_corrupt(-1, ECOR_TYPE);
	if (hdr->e_version == EV_NONE)
		return error_corrupt(-1, ECOR_VERSION);
	if (hdr->e_machine != EM_X86_64)
		return error_corrupt(-1, ECOR_MACHINE);

	if (hdr->e_phentsize > sizeof(Elf64_Phdr))
		error_warn(NORET, origin.name, ECOR_LPHENTSIZE);
	else if (hdr->e_phentsize < sizeof(Elf64_Phdr))
		return error_corrupt(-1, ECOR_SPHENTSIZE);
	
	if (hdr->e_shentsize > sizeof(Elf64_Shdr))
		error_warn(NORET, origin.name, ECOR_LSHENTSIZE);
	else if (hdr->e_shentsize < sizeof(Elf64_Shdr))
		return error_corrupt(-1, ECOR_SSHENTSIZE);

	if (check_corrupt(hdr->e_phoff, sizeof(Elf64_Phdr) * hdr->e_phnum))
		return error_corrupt(-1, ECOR_PHNUM);
	if (check_corrupt(hdr->e_shoff, sizeof(Elf64_Shdr) * hdr->e_shnum))
		return error_corrupt(-1, ECOR_SHNUM);

	return 0;
}
